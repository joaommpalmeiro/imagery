# square2round

<div align="center">
  <img alt="" src="./example.png" width="100" height="100" />
  <img alt="" src="./example_round.png" width="100" height="100" />
</div>

## Development

```bash
nvm install && nvm use && node --version
```

```bash
npm install
```

```bash
npm run dev
```

## References

- https://sharp.pixelplumbing.com/

## Notes

- `npm install sharp && npm install -D typescript ts-node @tsconfig/node18 prettier @feedzai/prettier-config @types/sharp`
