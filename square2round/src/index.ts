import sharp from "sharp";
import { Buffer } from "node:buffer";
import path from "node:path";

const INPUT_IMG = "logo.png";
const OUTPUT_IMG = `${path.parse(INPUT_IMG).name}_round.png`;
// console.log(OUTPUT_IMG);

// https://nodejs.org/api/buffer.html#new-bufferarray
// https://nodejs.org/api/buffer.html#static-method-bufferfromarray
// https://nodejs.org/api/buffer.html#static-method-bufferfromstring-encoding
const getOverlay = (width: number, height: number): Buffer => {
    const svgString = `<svg><rect x="0" y="0" width="${width}" height="${height}" rx="50%" /></svg>`;
    // console.log(svgString);

    return Buffer.from(svgString);
};

// https://github.com/lovell/sharp/issues/565#issuecomment-247570484
// https://github.com/lovell/sharp/issues/565#issuecomment-526711228
// https://github.com/lovell/sharp/issues/1837
// https://www.npmjs.com/package/@types/sharp?activeTab=explore
// https://sharp.pixelplumbing.com/api-output
const main = async () => {
    const metadata = await sharp(INPUT_IMG).metadata();
    // console.log(metadata);

    if (metadata.width && metadata.height) {
        const overlay = getOverlay(metadata.width, metadata.height);

        sharp(INPUT_IMG)
            .composite([{ input: overlay, blend: "dest-in" }])
            .toFile(OUTPUT_IMG)
            .then((info) => console.log(info));
    }
};

main();
