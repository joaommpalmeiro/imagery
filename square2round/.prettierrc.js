module.exports = {
    ...require("@feedzai/prettier-config"),
    overrides: [
        {
            files: ["*.md"],
            options: {
                tabWidth: 2,
            },
        },
    ],
};
